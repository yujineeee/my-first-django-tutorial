# Generated by Django 2.0.4 on 2018-04-23 16:38

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0002_auto_20180424_0129'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='choice',
            name='created_at',
        ),
    ]
